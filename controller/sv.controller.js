function layThongTinTuForm() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(tenSv, maSv, email, matKhau, diemToan, diemLy, diemHoa);
}

//render array sv ra giao dien
function renderDSSV(svArr) {
  // console.log("svArr: ", svArr);
  var contentHTML = "";
  var n = svArr.length;
  for (var i = 0; i < n; i++) {
    var sv = svArr[i];
    var trContent = ` <tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
      <button class="btn btn-primary" onclick="xoaSinhVien('${
        sv.ma
      }')">Xóa</button>
      <button class="btn btn-success" onclick="suaSinhVien('${
        sv.ma
      }')">Sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent;
  }
  //
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
// tbodySinhVien();

function timKiemViTri(id, dssv) {
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.ma == id) {
      // console.log(typeof sv.ma);
      // console.log(typeof id);
      return i;
    }
  }
  return -1;
}
function showThongTinTuForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matkhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function timSinhVienTheoTen(txtTen, arr) {
  var n = arr.length;
  var arrTen = [];
  for (var i = 0; i < n; i++) {
    if (arr[i].ten == txtTen) {
      arrTen.push(arr[i]);
    }
  }
  return arrTen;
}
