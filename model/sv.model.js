function SinhVien(ten, ma, email, matkhau, toan, ly, hoa) {
  this.ten = ten;
  this.ma = ma;
  this.email = email;
  this.matkhau = matkhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;

  this.tinhDTB = function () {
    return (
      Math.round(((this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3) * 100) / 100
    );
  };
}
