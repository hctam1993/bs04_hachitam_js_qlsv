const DSSV_LOCALSTORGE = "DSSV_LOCALSTORGE";

var dssv = [];
// lấy thông tin từ localStorge

var dssvJson = localStorage.getItem(DSSV_LOCALSTORGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    // console.log("sv: ", sv);
    dssv[i] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.email,
      sv.matkhau,
      sv.toan,
      sv.ly,
      sv.hoa
    );
    // console.log("dssv[i]: ", dssv[i]);
  }
  renderDSSV(dssv);
}

function themSV() {
  var newSv = layThongTinTuForm();

  //Mã SV
  var isValidMa =
    validator.kiemTraRong(newSv.ma, "spanMaSV", "Mã SV không được để rỗng.") &&
    validator.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã SV có độ dài 4 kí tự",
      4,
      4
    );
  //Tên SV
  var isValidTen =
    validator.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên SV không được để rỗng."
    ) &&
    validator.kiemTraDoDai(
      newSv.ten,
      "spanTenSV",
      "Tên SV phải có từ 2 đến 30 kí tự",
      2,
      30
    ) &&
    validator.kiemTraChiChu(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không có số."
    );
  //Email
  var isValidEmail =
    validator.kiemTraRong(
      newSv.email,
      "spanEmailSV",
      "Email không được để rỗng."
    ) &&
    validator.kiemTraEmail(newSv.email, "spanEmailSV", "Nhập đúng dạng email");
  //Pass
  var isValidPass =
    validator.kiemTraRong(
      newSv.matkhau,
      "spanMatKhau",
      "Mật khẩu không được để rỗng."
    ) &&
    validator.kiemTraDoDai(
      newSv.matkhau,
      "spanMatKhau",
      "Mật khẩu có 8 kí tự",
      8,
      8
    );
  //Toán
  var isValidToan =
    validator.kiemTraRong(
      newSv.toan,
      "spanToan",
      "Điểm Toán không được để rỗng."
    ) &&
    validator.kiemTraChiSo(newSv.toan, "spanToan", "Điểm Toán chỉ có số") &&
    validator.kiemtraDiem(newSv.toan, "spanToan", "Điểm phải > 0 và < 10");
  //Lý
  var isValidLy =
    validator.kiemTraRong(newSv.ly, "spanLy", "Điểm Lý không được để rỗng.") &&
    validator.kiemTraChiSo(newSv.ly, "spanLy", "Điểm Lý chỉ có số") &&
    validator.kiemtraDiem(newSv.ly, "spanLy", "Điểm phải > 0 và < 10");
  //Hóa
  var isValidHoa =
    validator.kiemTraRong(
      newSv.hoa,
      "spanHoa",
      "Điểm Hóa không được để rỗng."
    ) &&
    validator.kiemTraChiSo(newSv.hoa, "spanHoa", "Điểm Hóa chỉ có số") &&
    validator.kiemtraDiem(newSv.hoa, "spanHoa", "Điểm phải >= 0 và <= 10");
  // console.log("isValid: ", isValid);
  if (
    isValidMa &&
    isValidTen &&
    isValidEmail &&
    isValidPass &&
    isValidToan &&
    isValidLy &&
    isValidHoa
  ) {
    dssv.push(newSv);

    // console.log("dssv: ", dssv);

    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // lưu json vào localStorge
    localStorage.setItem(DSSV_LOCALSTORGE, dssvJson);

    renderDSSV(dssv);
  }
}

//xoa
function xoaSinhVien(id) {
  // var index = dssv.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  console.log(id);
  var index = timKiemViTri(id, dssv);

  if (index != -1) {
    // console.log(index);
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}
function reset() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

var index = -1; //vị trí sv

function suaSinhVien(id) {
  index = timKiemViTri(id, dssv);
  document.getElementById("txtMaSV").disabled = true;
  document.getElementById("btn_capnhat").disabled = false;
  document.getElementById("btn_them").disabled = true;
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinTuForm(sv);
  }
}
function capnhatSV() {
  document.getElementById("txtMaSV").disabled = false;
  if (index >= 0) {
    dssv[index].ten = document.getElementById("txtTenSV").value;
    dssv[index].email = document.getElementById("txtEmail").value;
    dssv[index].matkhau = document.getElementById("txtPass").value;
    dssv[index].toan = document.getElementById("txtDiemToan").value;
    dssv[index].ly = document.getElementById("txtDiemLy").value;
    dssv[index].hoa = document.getElementById("txtDiemHoa").value;
    renderDSSV(dssv);
    reset();
    document.getElementById("btn_capnhat").disabled = true;
    document.getElementById("btn_them").disabled = false;
  }
}
function showTenTim() {
  var ten = document.getElementById("txtSearch").value;
  // console.log("ten: ", ten);
  var arrTen = timSinhVienTheoTen(ten, dssv);
  console.log("arrTen: ", arrTen);
  if (arrTen.length >= 1) {
    renderDSSV(arrTen);
  } else {
    document.getElementById(
      "tbodySinhVien"
    ).innerHTML = `<h4>Không có sinh viên nào tên này</h4>`;
  }
}
